<?php
/**
 * @author     mediahof, Kiel-Germany
 * @link       http://www.mediahof.de
 * @copyright  Copyright (C) 2013 - 2014 mediahof. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

JFormHelper::loadFieldClass('list');

class JFormFieldModule extends JFormFieldList
{

    public $type = 'Module';

    protected function getOptions()
    {
        static $options = array();

        if (empty($options)) {
            $db = JFactory::getDbo();

            $query = $db->getQuery(true)
                ->select('m.module AS value')
                ->select('m.title AS text')
                ->from('#__modules AS m')
                ->where('m.published = ' . $db->Quote(1))
                ->where('m.client_id = ' . $db->Quote(0))
                ->where('m.module != ' . $db->Quote('mod_mh_simple_seasons'))
                ->order('m.title');

            $db->setQuery($query);

            $options = $db->loadObjectList();

            foreach ($options as $option) {
                $option->value = $option->value . '|' . $option->text;
            }

            array_unshift($options, JHtml::_('select.option', '0', JText::_('JSELECT')));
        }

        return array_merge(parent::getOptions(), $options);
    }
}