<?php
/**
 * @author     mediahof, Kiel-Germany
 * @link       http://www.mediahof.de
 * @copyright  Copyright (C) 2013 - 2014 mediahof. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';

$result = mod_mh_simple_seasons::_($params);

if (empty($result) || $result[0] == $module->module) {
    return;
}

$module->title = $result[1];

echo JFactory::getDocument()->loadRenderer('module')->render($result[0], array('title' => $result[1]));