<?php
/**
 * @author     mediahof, Kiel-Germany
 * @link       http://www.mediahof.de
 * @copyright  Copyright (C) 2013 - 2014 mediahof. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

defined('_JEXEC') or die;

abstract class mod_mh_simple_seasons
{

    public static function _(JRegistry &$params)
    {
        $season = $params->get('fixate') ? $params->get($params->get('fixate')) : $params->get(self::season($params));

        if (empty($season)) {
            return false;
        }

        return explode('|', $season);
    }

    private static function season(JRegistry &$params)
    {
        $season_names = array('winter', 'spring', 'summer', 'fall');

        $now = JFactory::getDate('now', 'UTC')->toUnix();

        $year = date('Y', $now);

        $diff = $params->get('day_diff') ? ' ' . $params->get('day_diff') . ' days' : '';

        switch ($params->get('hemisphere', 'northern')) {
            case 'northern':
                if ($now < strtotime($year . '-03-21' . $diff) || $now >= strtotime($year . '-12-21' . $diff)) {
                    return $season_names[0]; // Must be in Winter
                } elseif ($now >= strtotime($year . '-09-23' . $diff)) {
                    return $season_names[3]; // Must be in Fall
                } elseif ($now >= strtotime($year . '-06-21' . $diff)) {
                    return $season_names[2]; // Must be in Summer
                } elseif ($now >= strtotime($year . '-03-21' . $diff)) {
                    return $season_names[1]; // Must be in Spring
                }
                break;
                break;

            case 'southern':
                if ($now < strtotime($year . '-03-21' . $diff) || $now >= strtotime($year . '-12-21' . $diff)) {
                    return $season_names[2]; // Must be in Summer
                } elseif ($now >= strtotime($year . '-09-23' . $diff)) {
                    return $season_names[1]; // Must be in Spring
                } elseif ($now >= strtotime($year . '-06-21' . $diff)) {
                    return $season_names[0]; // Must be in Winter
                } elseif ($now >= strtotime($year . '-03-21' . $diff)) {
                    return $season_names[3]; // Must be in Fall
                }
                break;
                break;

            case 'australia':
                if ($now < strtotime($year . '-03-01' . $diff) || $now >= strtotime($year . '-12-01' . $diff)) {
                    return $season_names[2]; // Must be in Summer
                } elseif ($now >= strtotime($year . '-09-01' . $diff)) {
                    return $season_names[1]; // Must be in Spring
                } elseif ($now >= strtotime($year . '-06-01' . $diff)) {
                    return $season_names[0]; // Must be in Winter
                } elseif ($now >= strtotime($year . '-03-01' . $diff)) {
                    return $season_names[3]; // Must be in Fall
                }
                break;
                break;
        }
    }
}